package com.example.test.task.controller;

import com.example.test.task.dto.StackExchangeDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class SearchController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/search")
    public String searchAction(
            @RequestParam(value = "search", defaultValue = "", required = false) String search,
            Model model) throws JsonProcessingException {
        if (!search.isEmpty()){
            StackExchangeDTO exchangeData = getExchangeList(search);
            model.addAttribute("items", exchangeData.getItems());
            model.addAttribute("search", search);
        }
        return "search";
    }

    StackExchangeDTO getExchangeList(String search) throws JsonProcessingException {
        String fooResourceUrl = String.format("https://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle=%s&site=stackoverflow", encodeValue(search)); ;
        ResponseEntity<String> response = restTemplate.exchange(fooResourceUrl, HttpMethod.GET, null, String.class);

        if (response!=null) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            return mapper.readValue(response.getBody(), StackExchangeDTO.class);
        } else return new StackExchangeDTO();
    }

    // Method to encode a string value using `UTF-8` encoding scheme
    private static String encodeValue(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }
}
